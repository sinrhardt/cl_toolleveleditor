using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using UnityEngine.Serialization;


public class RoomDisplacer : EditorWindow
{
    [MenuItem("Tools/RoomDisplacer")]
    public static void OpenWindow() => EditorWindow.GetWindow(typeof(RoomDisplacer));


    public float snapRadius = 7.5f;
    public Material material;
    
    private GameObject[] _RoomPrefabs;
    private GameObject _currentRoom;

    private MeshAndMatrix[] _currentMesh;
    
    private SerializedObject _serializedObject;
    private SerializedProperty _snapRadiusProperty;
    private SerializedProperty _snapLayerMaskProperty;
    private SerializedProperty _serializedMaterialProperty;

    private Quaternion _roomRotation;
    private GameObject _spawnedRooms;
    
    private void OnEnable()
    {
        SceneView.duringSceneGui += DuringSceneGUI;
            
        _roomRotation = Quaternion.identity;
        _serializedObject = new SerializedObject(this);
        _snapRadiusProperty = _serializedObject.FindProperty(nameof(snapRadius));
        _serializedMaterialProperty = _serializedObject.FindProperty(nameof(material));
        
        // Get prefabs
        string[] guids = AssetDatabase.FindAssets("t:prefab", new[] { "Assets/Prefabs" });
        IEnumerable<string> paths = guids.Select(AssetDatabase.GUIDToAssetPath);
        _RoomPrefabs = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();

        _spawnedRooms = new GameObject("Map");


    }
    
    private void OnDisable()
    {
        SceneView.duringSceneGui -= DuringSceneGUI;
    }

    private void OnGUI()
    {
        _serializedObject.Update();
        
        EditorGUILayout.LabelField("CTRL + E/Q to rotate preview", EditorStyles.boldLabel);
        EditorGUILayout.Space();
        
        EditorGUILayout.PropertyField(_snapRadiusProperty);
        _snapRadiusProperty.floatValue = _snapRadiusProperty.floatValue.AtLeast(1.0f);
        EditorGUILayout.Space();


        
        EditorGUILayout.PropertyField(_serializedMaterialProperty);
        EditorGUILayout.Space();

        if (GUILayout.Button("Undo last room"))
        {
            Undo.PerformUndo();
        }
        EditorGUILayout.Space();
        if (GUILayout.Button("Redo last room"))
        {
            Undo.PerformRedo();
        }
        EditorGUILayout.Space();
        
        if (GUILayout.Button("SaveCurrentMap"))
        {
            SaveMap();
        }
        EditorGUILayout.Space();
        


        if (_serializedObject.ApplyModifiedProperties())
        {
            SceneView.RepaintAll();
        }


        if (Event.current.type != EventType.MouseDown || Event.current.button != 0) return;
        GUI.FocusControl(null);
        Repaint();
    }


    private void DuringSceneGUI(SceneView sceneView)
    {
        DrawGUI();
        if (_currentRoom == null) return;

        // PREVIEW
        var pos = PreviewPrefab();
        Handles.DrawWireDisc(pos, Vector3.up, snapRadius);
        
        // ROTATION
        RotatePrefab();
        
        // SPAWN
        if (Event.current.keyCode == KeyCode.Space && Event.current.type == EventType.KeyUp)
        {
            SpawnPrefab(pos);
        }
        
        
        
        
        
        if (Event.current.type == EventType.MouseMove)
        {
            sceneView.Repaint();
        }
    }

    private Vector3 PreviewPrefab()
    {
        // Mouse calcs
        var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        var planeToHit = new Plane(Vector3.up, Vector3.zero);
        var mouseHit = planeToHit.Raycast(ray, out float distance) ? ray.GetPoint(distance) : Vector3.zero;

        mouseHit = CheckSnap(mouseHit);
        
        // Render material and mesh of prefab
        if (material != null)
            material.SetPass(0);
        foreach (var mesh in _currentMesh)
        {
            
            Matrix4x4 childToWorldMatrix = Matrix4x4.TRS(mouseHit, _roomRotation, Vector3.one) * mesh.localToWorldMatrix;
            Graphics.DrawMeshNow(mesh.mesh, childToWorldMatrix);
        }

        return mouseHit;
    }

    private Vector3 CheckSnap(Vector3 pos)
    {

        Collider[] points = Physics.OverlapSphere(pos, snapRadius, LayerMask.NameToLayer("Snap"));

        if (points == null || points.Length == 0) return pos;
        
        Collider closest = points[0];
        if (points.Length > 1)
        {
            foreach (var VARIABLE in points)
            {
                if (Vector3.Distance(VARIABLE.transform.position, pos) < Vector3.Distance(closest.transform.position, pos))
                {
                    closest = VARIABLE;
                }
            }
        }

        if (Vector3.Distance(closest.transform.parent.transform.position, pos) < 5) return pos;

        var point = closest.transform.position;
        var closestParent = closest.transform.parent.transform.position;
        
        var x = Math.Abs(pos.x - point.x);
        var z = Math.Abs(pos.z - point.z);
        
        if (x >= z)
        {
            pos.z = closestParent.z;
            pos.x = closestParent.x + (10.0f * Mathf.Sign(pos.x - point.x));
        }
        else
        {
            pos.z = closestParent.z + (10.0f * Mathf.Sign(pos.z - point.z));
            pos.x = closestParent.x;

        }

        return pos;
    }
    
    private void RotatePrefab()
    {
        bool controlKeyDown = (Event.current.modifiers & EventModifiers.Control) != 0;
        if (!controlKeyDown || Event.current.type != EventType.KeyUp) return;
        if (Event.current.keyCode == KeyCode.Q)
        {
            _roomRotation *= Quaternion.AngleAxis(-90, Vector3.up);
        }
        else if (Event.current.keyCode == KeyCode.E)
        {
            _roomRotation *= Quaternion.AngleAxis(90, Vector3.up);
        }
    }

    private void SpawnPrefab(Vector3 pos)
    {
        GameObject roomToSpawn = (GameObject)PrefabUtility.InstantiatePrefab(_currentRoom);
        Undo.RegisterCreatedObjectUndo(roomToSpawn, roomToSpawn.name);
        roomToSpawn.transform.SetPositionAndRotation(pos, _roomRotation);
        roomToSpawn.transform.SetParent(_spawnedRooms.transform);
    }
    
    private void DrawGUI()
    {
        Handles.BeginGUI();
        if (_RoomPrefabs == null || _RoomPrefabs.Length == 0) return;
        // Draw prefabs
        
        Rect rect = new Rect(8, 8, 70, 70);


        for (var index = 0; index < _RoomPrefabs.Length; index++)
        {
            Texture icon = AssetPreview.GetAssetPreview(_RoomPrefabs[index]);

            bool buttonClick = GUI.Button(rect, icon);


            if (buttonClick)
            {
                _currentRoom = _RoomPrefabs[index];

                // Save room mesh and mesh
                MeshFilter[] filters = _currentRoom.GetComponentsInChildren<MeshFilter>();
                _currentMesh = new MeshAndMatrix[filters.Length];
                
                for (int j = 0; j < filters.Length; j++)
                {
                    _currentMesh[j].mesh = filters[j].sharedMesh;
                    _currentMesh[j].localToWorldMatrix = filters[j].transform.localToWorldMatrix;
                }
            }

            rect.y += rect.height + 2;
        }

        Handles.EndGUI();

    }

    private struct MeshAndMatrix
    {
        public Mesh mesh;
        public Matrix4x4 localToWorldMatrix;
    }

    private void SaveMap()
    {
        PrefabUtility.SaveAsPrefabAsset(_spawnedRooms, "Assets/Prefabs/Maps/" + _spawnedRooms.name + ".prefab");
    }
}

